import pytest

from utils.utils import *

def test_returns_tail_of_list():
    items = [1, 2, 3]
    assert(bf(items)) == [2, 3]

def test_returns_first_of_list():
    items = [1, 2, 3]
    assert(first(items)) == [1]

def test_returns_tail_of_sentence():
    sentence = "one two three"
    assert(bf(sentence)) == "two three"

def test_returns_head_of_sentence():
    sentence = "one two three"
    assert(first(sentence)) == "one"
