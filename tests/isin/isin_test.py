import pytest

from spurious.isin import *

def test_it_works():
    list_of_words = ["got", "to", "get", "you"]
    sentence = "got to get you into my life"
    ret = contained_words(sentence, list_of_words)
    assert(ret.strip()) == "got to get you"
