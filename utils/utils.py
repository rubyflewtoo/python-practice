def bf(item):
    if type(item) is list:
        return item[1:]
    if type(item) is str:
        # split the string
        tail_sentence = item.split(" ")[1:]
        return " ".join(tail_sentence)

def first(item):
    if type(item) is list:
        return item[0:1]
    if type(item) is str:
        head_sentence = item.split(" ")[0:1]
        return " ".join(head_sentence)
