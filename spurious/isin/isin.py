from utils.utils import bf, first

def contained_words(sentence, list_of_words):
    """return only the words in sentence that are contained in list_of_words"""
    if len(sentence) == 0:
        return ""
    else:
        if first(sentence) in list_of_words:
           return first(sentence) + " " + contained_words(bf(sentence), list_of_words)
        else:
           return contained_words(bf(sentence), list_of_words)
